# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Maintainer: Henrik Riomar <henrik.riomar@gmail.com>
pkgname=nats-server
pkgver=2.9.1
pkgrel=0
pkgdesc="High-Performance server for NATS.io"
url="https://github.com/nats-io/nats-server"
arch="all !x86 !armv7 !armhf !s390x !ppc64le" # limited by failing check()
options="!check" # unstable
license="Apache-2.0"
makedepends="go"
source="https://github.com/nats-io/nats-server/archive/v$pkgver/nats-server-$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build
}

check() {
	go test -v -p=1 -run=TestNoRace ./server -count=1 -vet=off -timeout=30m -failfast
}

package() {
	install -Dm755 nats-server "$pkgdir"/usr/bin/nats-server
}

sha512sums="
d9ef721a07e4ccc6fb1cc005645af293cdbac1385101e9645353add9332895a6b5cab148d895e1314d41eda333cc0f915c8f185988d0c901f78aea8b2bcaec6f  nats-server-2.9.1.tar.gz
"
