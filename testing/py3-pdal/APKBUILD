# Contributor: Bradley J Chambers <brad.chambers@gmail.com>
# Maintainer: Bradley J Chambers <brad.chambers@gmail.com>
pkgname=py3-pdal
pkgver=3.1.2
pkgrel=0
pkgdesc="PDAL Python bindings"
options="!check" # disabled - failing test with circular import
url="https://github.com/pdal/python"
arch="all"
license="BSD-3-Clause"
depends="python3 py3-numpy"
checkdepends="pdal py3-pytest"
makedepends="python3-dev pdal-dev py3-scikit-build py3-numpy-dev cmake ninja py3-pybind11-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/PDAL/python/archive/$pkgver.tar.gz"
builddir="$srcdir/python-$pkgver"

replaces="py-pdal" # Backwards compatibility
provides="py-pdal=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build -DWITH_TESTS=ON
}

check() {
	export PYTHONPATH=$PYTHONPATH:$(python -c "import os, skbuild; print(os.path.join('plugins', skbuild.constants.SKBUILD_DIR(), 'cmake-build'))")
	py.test -v test/
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
6cb4f9cc0bc80068799fcb28b0fdaeb9c7c354e72cb37349e6fd77645c1e16ed9a06ef835f6da6f4b70d4981901a166c245cbdac02d54e877ee6d36642c8990c  py3-pdal-3.1.2.tar.gz
"
