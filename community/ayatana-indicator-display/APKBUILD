# Contributor: Luca Weiss <luca@z3ntu.xyz>
# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=ayatana-indicator-display
pkgver=22.9.0
pkgrel=0
pkgdesc="Ayatana Indicator Display"
url="https://github.com/AyatanaIndicators/ayatana-indicator-display"
arch="all"
license="GPL-3.0-only"
makedepends="
	cmake
	cmake-extras
	glib-dev
	intltool
	libayatana-common-dev
	libgudev-dev
	properties-cpp-dev
	"
subpackages="$pkgname-lang"
source="https://github.com/AyatanaIndicators/ayatana-indicator-display/archive/$pkgver/ayatana-indicator-display-$pkgver.tar.gz"
options="!check" # requires unpackaged dependencies (at least libqtdbusmock & libqtdbustest)

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		$CMAKE_CROSSOPTS .
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
db17e7c40f08f38ad509711468b1248af3dd0aa30149fb479487ea8cff1f243d460ae2491200c3581040324de43fe65ca1d31d51a96bf542e2db40f7552477dd  ayatana-indicator-display-22.9.0.tar.gz
"
