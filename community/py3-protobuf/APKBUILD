# Contributor: Corentin Henry <corentinhenry@gmail.com>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-protobuf
pkgver=4.21.6
pkgrel=0
pkgdesc="Google's data interchange format"
url="https://github.com/protocolbuffers/protobuf"
arch="noarch"
license="BSD-3-Clause"
depends="py3-six>=1.9 tzdata"
makedepends="protobuf-dev py3-setuptools py3-wheel"
checkdepends="py3-pytest"
# NOTE: Always use releases of protobuf, not pypi or auto-generated GitHub
# tarballs as they do not contain the necessary components to run tests.
source="https://github.com/protocolbuffers/protobuf/releases/download/v21.6/protobuf-python-$pkgver.tar.gz"
builddir="$srcdir"/protobuf-$pkgver/python

replaces="py-protobuf" # Backwards compatibility
provides="py-protobuf=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

check() {
	pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	# correct permissions
	chmod +r "$pkgdir"/usr/lib/*/site-packages/*/*
}

sha512sums="
a1a1bc9c999767a35ed352b6d85392d5a32cf7aeb745cc7f6036e199df948e55525229812470fda01c3b7f28bc9e0d51adab4abf3906e63a0ad948102aafb793  protobuf-python-4.21.6.tar.gz
"
